import './App.css';
import { Route, Switch } from "react-router-dom";


import Login from './pages/login';
import Profile from './pages/profile';
import Translation from './pages/translation';
import NavBar from './components/navbar';

function App() {
  return (
    <div className="App">
      <NavBar/>
      <Switch>
        <Route path="/" exact component={Login}/>
        <Route path="/profile" exact component={Profile}/>
        <Route path="/translation" exact component={Translation}/>
      </Switch>
    </div>
  );
}

export default App;
